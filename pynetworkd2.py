import sys,configparser,logging,ipaddress,socket
import pyroute2




def parse_netlink_addrs(addrs):
    ret = []
    for netreply in addrs:
        addr = dict(netreply['attrs'])['IFA_ADDRESS']
        prefixlen = netreply['prefixlen']
        iface = ipaddress.ip_interface('%s/%d' % (addr, prefixlen))
        ret.append(iface)
    return ret


class Interface(object):
    def __init__(self, config, iface_name, iface_type=None):
        self.iface_name = iface_name
        self.iface_type = iface_type
        self.iface_cfg = config[iface_name]
        if 'addresses' in self.iface_cfg:
            self.addresses = set(map(ipaddress.ip_interface, map(str.strip, self.iface_cfg['addresses'].split(','))))
        else:
            self.addresses = None
        self.vrf = self.iface_cfg.get('vrf', None)
        self.mtu = None
        if 'mtu' in self.iface_cfg:
            self.mtu = int(self.iface_cfg['mtu'])

    def ensure_exists(self, ip):
        logging.info('Ensuring %s device %s exists..', self.iface_type, self.iface_name)
        ret = ip.link_lookup(ifname=self.iface_name)
        if len(ret) == 0:
            if self.iface_type is None:
                logging.info('Device is absent and dont know how to create.. ignoring')
                self.iface_idx = None
                return
            logging.info('Creating %s device..', self.iface_type)
            self.create_iface(ip)
        self.iface_idx = ip.link_lookup(ifname=self.iface_name)[0]

    def ensure_addrs(self, ip):
        if self.addresses is None:
            return
        # ignore missing deviees
        if self.iface_idx is None:
            return
        # TODO: implement removal of addresses
        prev_addrs = set(parse_netlink_addrs(ip.get_addr(index=self.iface_idx)))
        for addr in self.addresses:
            if addr in prev_addrs:
                continue
            ip.addr('add', index=self.iface_idx, address=str(addr.ip), mask=addr.network.prefixlen)

    def bring_up(self, ip):
        if self.iface_idx is not None:
            ip.link('set', index=self.iface_idx, state='up')
            if self.mtu:
                ip.link('set', index=self.iface_idx, mtu=self.mtu)
            
    

class WireguardPeer(object):
    def __init__(self, config, iface_name, peer_name):
        self.iface_name = iface_name
        self.peer_name = peer_name
        selector = iface_name + ':' + peer_name
        self.public_key = config[selector]['publickey']
        self.endpoint = config[selector].get('endpoint', None)

    def to_peer_dict(self):
        ret = {
            'public_key': self.public_key,
            # TODO: make allowed ips configurable
            'allowed_ips': ['::/0', '0.0.0.0/0']
        }
        if self.endpoint is not None:
            if 'dns:' in self.endpoint:
                prefix, hostname, port = self.endpoint.split(':')
                addrs = socket.getaddrinfo(hostname, 41000, socket.AF_INET6, socket.SOCK_STREAM)
                if len(addrs) >= 0:
                    ret['endpoint_addr'] = addrs[0][4][0]
                    ret['endpoint_port'] = int(port)
                else:
                    raise ValueError('Hostname lookup for %s failed' % self.endpoint)
            else:
                ret['endpoint_addr'] = self.endpoint.split(':')[0]
                ret['endpoint_port'] = int(self.endpoint.split(':')[1])
        return ret

class WireguardInterface(Interface):
    def __init__(self, config, iface_name):
        super(WireguardInterface, self).__init__(config, iface_name, iface_type='wireguard')
        private_key_file = self.iface_cfg['private_key_file']
        with open(private_key_file, 'r') as f:
            self.private_key = f.read()
        self.port = int(self.iface_cfg['port'])

        # find the peers
        section_selector = iface_name + ':'
        self.peers = []
        for section in config.sections():
            if not section.startswith(section_selector):
                continue
            _, peer_name = section.split(':')
            peer = WireguardPeer(config, iface_name, peer_name)
            self.peers.append(peer)
        
    
class VRFInterface(object):
    def __init__(self, config, iface_name):
        self.iface_type = 'vrf'
        self.iface_name = iface_name
        self.vrf_table_num = int(config[iface_name]['vrf_table'])

class DummyInterface(Interface):
    def __init__(self, config, iface_name):
        super(DummyInterface, self).__init__(config, iface_name, iface_type='dummy')

    def create_iface(self, ip):
        ip.link('add', ifname=self.iface_name, kind='dummy')

class BridgeInterface(Interface):
    def __init__(self, config, iface_name):
        super(BridgeInterface, self).__init__(config, iface_name, iface_type='bridge')

    def create_iface(self, ip):
        ip.link('add', ifname=self.iface_name, kind='bridge')
        
TYPE_CLASS_MAP = {
    'wireguard': WireguardInterface,
    'vrf': VRFInterface,
    'dummy': DummyInterface,
    'bridge': BridgeInterface
}


def parse_interfaces(config):
    ifaces = {}
    for section in config.sections():
        # ignore sub-interface configurations
        # (such as wireguard peers, etc.)
        if ':' in section:
            continue
        if section.startswith('.'):
            continue
        if len(section) > 16:
            logging.error('Interface name "%s" is too long! Ignoring', section)
            continue
        
        logging.debug('Parsing interface %s', section)

        if 'type' in config[section]:
            iface_class = TYPE_CLASS_MAP[config[section]['type']]
        else:
            iface_class = Interface

        iface = iface_class(config, section)
        ifaces[section] = iface
    return ifaces


def handle_vrf(ip, vrf_iface):
    logging.info('Ensuring VRF %s exists..', vrf_iface.iface_name)
    # check if it exists
    ret = ip.link_lookup(ifname=vrf_iface.iface_name)
    if len(ret) == 1:
        ip.link('set', index=ret[0], state='up')
        # TODO: check if it is an VRF
        # TODO: check if the tablenum is correct
        logging.info('Already exists - Nothing to do')
        return ret[0]

    logging.info('Creating with tablenum=%d..', vrf_iface.vrf_table_num)
    ip.link('add', ifname=vrf_iface.iface_name, kind='vrf', vrf_table=vrf_iface.vrf_table_num)
    
    # this should work now..
    ret = ip.link_lookup(ifname=vrf_iface.iface_name)[0]
    ip.link('set', index=ret, state='up')

def handle_wireguard(ip, iface):
    ifname = iface.iface_name
    
    logging.info('Ensuring WG device %s exists..', ifname)
    ret = ip.link_lookup(ifname=ifname)
    if len(ret) == 0:
        logging.info('Creating wireguard device..')
        ip.link('add', ifname=ifname, kind='wireguard')

    wg = pyroute2.WireGuard()
    wg_info = dict(wg.info(ifname)[0]['attrs'])

    # check port if set
    if iface.port is not None:
        if wg_info['WGDEVICE_A_LISTEN_PORT'] != iface.port:
            logging.info('Setting port to %d', iface.port)
            wg.set(ifname, listen_port=iface.port)

    # ensure the private key is set
    if wg_info.get('WGDEVICE_A_PRIVATE_KEY', b'') != iface.private_key:
        wg.set(ifname, private_key=iface.private_key)

    # just set the peers..
    for peer in iface.peers:
        peer_cfg = peer.to_peer_dict()
        wg.set(ifname, peer=peer_cfg)


# TODO: integrate this into Interface class
def handle_vrf_membership(ip, iface, vrf_indexes):
    # FIXME: how do we unlink a VRF?
    if iface.vrf is not None:
        vrfidx = vrf_indexes[iface.vrf]
    else:
        return
        
    ifname = iface.iface_name
    ret = ip.link_lookup(ifname=ifname)
    if len(ret) == 0:
        return
    ifidx = ret[0]
    attrs = ip.link('get', index=ifidx)[0]
    if attrs.get('IFLA_MASTER', None) != vrfidx:
        ip.link('set', index=ifidx, master=vrfidx)


def handle_rule(ip, rule):
    rule_num, family, kwargs = rule
    existing_rules = ip.get_rules(family=family)
    kwargs['priority'] = rule_num

    # FIXME: refactor so we only do this parsing once instead of ~N/2 times..
    # parse existing rules
    def parse_rule(a):
        priority = 0
        if 'FRA_PRIORITY' in dict(a['attrs']):
            priority = dict(a['attrs'])['FRA_PRIORITY']
        return priority
    existing_rules = list(map(parse_rule, existing_rules))
    
    if rule_num in existing_rules:
        # TODO: check if they are actually different instead of removing and readding it
        ip.rule('delete', family=family, priority=rule_num)
    ip.rule('add', family=family, **kwargs)

def parse_rules(config):
    if '.rules' not in config:
        return []
    ret = []
    for rule_num in config.options('.rules'):
        rule_descr = config['.rules'][rule_num].strip().split(' ')
        rule_num = int(rule_num)

        kwargs = {}

        family = socket.AF_INET6
        
        while len(rule_descr) > 0:
            key = rule_descr.pop(0)
            if key == 'from':
                prefix = rule_descr.pop(0)
                network = ipaddress.ip_network(prefix)
                netaddr = str(network.network_address)
                netlen = network.prefixlen
                kwargs['src'] = netaddr
                kwargs['src_len'] = netlen
            elif key == 'to':
                prefix = rule_descr.pop(0)
                network = ipaddress.ip_network(prefix)
                netaddr = str(network.network_address)
                netlen = network.prefixlen
                kwargs['dst'] = netaddr
                kwargs['dst_len'] = netlen
            elif key == 'iif':
                ifname = rule_descr.pop(0)
                kwargs['iifname'] = ifname
            elif key == 'oif':
                ifname = rule_descr.pop(0)
                kwargs['oifname'] = ifname
            elif key == 'table':
                table = rule_descr.pop(0)
                if table == 'main':
                    table = 254
                kwargs['table'] = int(table)
            elif key == 'inet6':
                family = socket.AF_INET6
            elif key == 'inet4':
                family = socket.AF_INET4
            elif key == 'delete':
                # TODO: add support for removing rules again
                pass
            else:
                raise ValueError('Unknown rule key %s in rule %s' % (key, repr(config['.rules'][rule_num])))

        rule = (rule_num, family, kwargs)
        ret.append(rule)
    return ret

                
        
def main(config_files):
    config = configparser.ConfigParser()
    # TODO: ensure the config files exist
    config.read(config_files, encoding='utf-8')

    interfaces = parse_interfaces(config)

    ip = pyroute2.IPRoute()

    # ensure all the VRFs are there before we continue
    # (plus get their index)
    vrf_ifaces = dict(filter(lambda i: i[1].iface_type == 'vrf', interfaces.items()))
    vrf_indexes = {}
    for vrf_iface in vrf_ifaces.values():
        idx = handle_vrf(ip, vrf_iface)
        vrf_indexes[vrf_iface.iface_name] = idx

    wg_ifaces = dict(filter(lambda i: i[1].iface_type == 'wireguard', interfaces.items()))
    for wg_iface in wg_ifaces.values():
        handle_wireguard(ip, wg_iface)

    not_vrfs = dict(filter(lambda i: i[1].iface_type != 'vrf', interfaces.items()))
    for iface in not_vrfs.values():
        handle_vrf_membership(ip, iface, vrf_indexes)
        iface.ensure_exists(ip)
        iface.ensure_addrs(ip)
        iface.bring_up(ip)

    rules = parse_rules(config)
    for rule in rules:
        handle_rule(ip, rule)
    

if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.stderr.write('Usage: pynetworkd2 <config file> [config file ..]\n')
        sys.exit(1)
    main(sys.argv[1:])
