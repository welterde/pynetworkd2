from distutils.core import setup

setup(name='pynetworkd2',
      author='Tassilo Schweyer',
      author_email='dev+pynetworkd2@welterde.de',
      version='0.1',
      scripts=[
          'pynetworkd2'
      ])
