
# Usage

Example *pynetworkd.ini* configuration (see examples/ for more examples):
```
[vrf1]
type = vrf

[lo.vrf1]
type = dummy
addresses = 2001:db::1/128
vrf = vrf1

[eth0]
vrf = vrf1
addresses = 2001:db8:42::42/64
```

And then it's just as simple as running pynetworkd:

```
pynetworkd2 pynetworkd.ini
```

# Installation

Debian package is being worked on.

# Roadmap

* Support removing interfaces again
* Support removing addresses
* Support ignoring certain interfaces (important when removing interfaces feature is on)
* Refactor code to get rid of random code duplicates, etc.
* Improve logging
* Add debug mode
* Finish debian package

# Anti-Features

Or features I deem do not belong into this software.

* Dynamic address configuration: Should be handled by software dedicated to this task such as dhcpcd, etc.
* Dealing with routes: Should be handled by dedicated routing daemon such as FRR or bird